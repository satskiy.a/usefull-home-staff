# MAC mini as a home Server  using  WIFY only

***

# Install Proxmox

First you need to install Proxmox
- update sourses list from and install kernel headers (proxmox-headers-)

```
# Proxmox VE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve bookworm pve-no-subscription
```
-  wify Drivers 
```
apt install broadcom-sta-dkms
```
- disable old (OPTIONAL)
```
root@pve:~# cat /etc/modprobe.d/blacklist
blacklist b43
blacklist brcmsmac
blacklist bcma
```

- use new
```
modprobe wl
```

## Connect  to  wify

- install wpasupplicant
```
apt install  wpasupplicant
```
- disable wpasupplicant service
- put in file /etc/wpa_supplicant/wpa_supplicant.conf
```
ctrl_interface=/run/wpa_supplicant
update_config=1
```

- start  on inerface
```
wpa_supplicant -B -i interface -c /etc/wpa_supplicant/wpa_supplicant.conf
```      
- then connect  using wpa_cli
```
wpa_cli
scan
scan_results
add_network
set_network 0 ssid "MYSSID"  (networks  starts from 0)
set_network 0 psk "passphrase"
enable_network 0
save_config
quit
```

- decode psk 
```
su -l -c "wpa_passphrase MYSSID passphrase > /etc/wpa_supplicant/wpa_supplicant.conf"
```

- re enable
```
systemctl reenable wpa_supplicant.service
```


## network Config

```
auto lo
iface lo inet loopback

iface enp1s0f0 inet manual

allow-hotplug wlp2s0
iface wlp2s0 inet dhcp
        wpa-ssid MYSSID
        wpa-psk 1d242e4f8ba215ec1935a000582ca8c5d3f467de570cc677816528981ce3cf55

# Virtual Bridge interface
auto vmbr0
iface vmbr0 inet static
    #Gateway for VMs
    address 192.168.200.1/24
    bridge-ports none
    bridge-stp off
    bridge-fd 0
    # Enable ip forwarding
    post-up echo 1 > /proc/sys/net/ipv4/ip_forward
    # Route all traffic from VMs / Container through the wireless interface hiding its internal IP
    post-up iptables -t nat -A POSTROUTING -s '192.168.200.0/24' -o wlp2s0 -j MASQUERADE
    post-down iptables -t nat -D POSTROUTING -s '192.168.200.0/24' -o wlp2s0 -j MASQUERADE
    #Firewall
    post-up iptables -t nat -A PREROUTING -i wlp2s0 -p tcp --dport 2222 -j DNAT --to-destination 192.168.200.100:22
    post-up iptables -t nat -A PREROUTING -i wlp2s0 -p tcp -m multiport --dports 8096,12321,80 -j DNAT --to-destination 192.168.200.100
```




## Install DuckDNS clie

- Create a DNS name and use duck dns cli on it
- set DMZ

